/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Client } from './client.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/base/base.service';

@Injectable()
export class ClientService extends BaseService<Client> {
  constructor(
    @InjectRepository(Client)
    protected readonly repository: Repository<Client>,
  ) {
    super(repository);
  }

  async findInRoom(roomId: string): Promise<Client[]> {
    return this.repository.find({ where: { roomId } });
  }
}
