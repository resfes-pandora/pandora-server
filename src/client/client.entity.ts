import { BaseEntity } from 'src/base/base.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class Client extends BaseEntity {
  @Column({ name: 'room-id' })
  roomId: string;
  
  @Column()
  examStatus: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  peerId: string;

  @Column()
  type: string;
}
