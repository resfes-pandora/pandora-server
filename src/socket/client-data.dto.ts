export default class ClientData {
  clientName?: string;
  clientType?: string;
  roomId?: string;
  message?: string;
  peerId?: string;
  notification?: any;
  clientId?: string;
  examStatus?: string;
}
