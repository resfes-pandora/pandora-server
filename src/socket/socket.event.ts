export const clientEvents = {
  JOIN_ROOM: 'JOIN_ROOM',
  SEND_CHAT_MESSAGE: 'SEND_CHAT_MESSAGE',
  SET_PEER_ID: 'SET_PEER_ID',
  WEBCAM_CONNECTED: 'WEBCAM_CONNECTED',
  SCREEN_CONNECTED: 'SCREEN_CONNECTED',
  PUSH_NOTIFICATION: 'PUSH_NOTIFICATION',
  UPDATE_EXAM_STATUS: 'UPDATE_EXAM_STATUS',
};

export const serverEvents = {
  NEW_CHAT_MESSAGE: 'NEW_CHAT_MESSAGE',
  ANOTHER_CLIENT_JOINED: 'ANOTHER_CLIENT_JOINED',
  ANOTHER_CLIENT_DISCONNECTED: 'ANOTHER_CLIENT_DISCONNECTED',
  NEW_PEER_CONNECTED: 'NEW_PEER_CONNECTED',
  NEW_WEBCAM_CONNECTED: 'NEW_WEBCAM_CONNECTED',
  NEW_SCREEN_CONNECTED: 'NEW_SCREEN_CONNECTED',
  NEW_NOTIFICATION: 'NEW_NOTIFICATION',
  NEW_EXAM_STATUS: 'NEW_EXAM_STATUS',
};
