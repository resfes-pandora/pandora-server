import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { Client } from 'src/client/client.entity';
import { ClientService } from 'src/client/client.service';
import { clientEvents, serverEvents } from 'src/socket/socket.event';
import ClientData from './client-data.dto';

@WebSocketGateway(+process.env.SOCKET_PORT)
export class SocketGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  public server: Server;

  constructor(private readonly clientService: ClientService) {
    this.clientService.clear();
  }

  afterInit(server: Server): any {
    console.log('Socket server initialized.');
  }

  handleDisconnect(socket: Socket): any {
    this.clientService
      .findOne(socket.id)
      .then(client => {
        socket
          .to(client.roomId)
          .emit(serverEvents.ANOTHER_CLIENT_DISCONNECTED, client);
        this.clientService.delete(socket.id);
      })
      .catch(error => console.log(error))
      .finally(() => {
        console.log(`Client ${socket.id} disconnected.`);
      });
  }

  handleConnection(socket: Socket, ...args: any[]): any {
    console.log(`Client ${socket.id} connected.`);
  }

  @SubscribeMessage(clientEvents.JOIN_ROOM)
  async clientJoinRoom(
    @MessageBody() data: ClientData,
    @ConnectedSocket() socket: Socket,
  ): Promise<Client[]> {
    const { roomId, clientName, clientType } = data;
    const client = new Client({
      id: socket.id,
      roomId,
      name: clientName,
      type: clientType,
      examStatus: clientType === "STUDENT" ? "NOT_YET_START" : "",
    });

    socket.join(roomId);
    socket.to(roomId).emit(serverEvents.ANOTHER_CLIENT_JOINED, client);

    await this.clientService.save(client);
    return this.clientService.findInRoom(roomId);
  }

  @SubscribeMessage(clientEvents.SEND_CHAT_MESSAGE)
  async clientSendChatMessage(
    @MessageBody() data: ClientData,
    @ConnectedSocket() socket: Socket,
  ): Promise<void> {
    const { message, roomId, clientName } = data;

    socket
      .to(roomId)
      .emit(serverEvents.NEW_CHAT_MESSAGE, { message, clientName });
  }

  @SubscribeMessage(clientEvents.SET_PEER_ID)
  async newPeerId(
    @MessageBody() data: ClientData,
    @ConnectedSocket() socket: Socket,
  ): Promise<void> {
    const { roomId, peerId } = data;

    const client = await this.clientService.findOne(socket.id);
    client.peerId = peerId;
    await this.clientService.save(client);

    socket
      .to(roomId)
      .emit(serverEvents.NEW_PEER_CONNECTED, { id: socket.id, peerId });
  }

  @SubscribeMessage(clientEvents.WEBCAM_CONNECTED)
  async newWebCamConnected(
    @MessageBody() data: ClientData,
    @ConnectedSocket() socket: Socket,
  ): Promise<void> {
    const { roomId } = data;

    socket
      .to(roomId)
      .emit(serverEvents.NEW_WEBCAM_CONNECTED, { id: socket.id });
  }

  @SubscribeMessage(clientEvents.SCREEN_CONNECTED)
  async newScreenConnected(
    @MessageBody() data: ClientData,
    @ConnectedSocket() socket: Socket,
  ): Promise<void> {
    const { roomId } = data;

    socket
      .to(roomId)
      .emit(serverEvents.NEW_SCREEN_CONNECTED, { id: socket.id });
  }

  @SubscribeMessage(clientEvents.PUSH_NOTIFICATION)
  async clientPushNotification(
    @MessageBody() data: ClientData,
    @ConnectedSocket() socket: Socket,
  ): Promise<void> {
    const { roomId, notification } = data;

    socket.to(roomId).emit(serverEvents.NEW_NOTIFICATION, { notification });
  }

  @SubscribeMessage(clientEvents.UPDATE_EXAM_STATUS)
  async updateExamStatus(
    @MessageBody() data: ClientData,
    @ConnectedSocket() socket: Socket,
  ): Promise<void> {
    const { roomId, clientId, examStatus } = data;

    let client = await this.clientService.findOne({ id: clientId })
    client.examStatus = examStatus;
    this.clientService.save(client);
    socket.to(roomId).emit(serverEvents.NEW_EXAM_STATUS, { clientId, examStatus });
  }
}
