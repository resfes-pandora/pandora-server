/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { BaseEntity } from './base.entity';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

@Injectable()
export class BaseService<T extends BaseEntity> extends TypeOrmCrudService<T> {
  constructor(protected readonly repository: Repository<T>) {
    super(repository);
  }

  async save(entity: any): Promise<BaseEntity> {
    return this.repository.save(entity);
  }

  async delete(id: string): Promise<any> {
    return this.repository.delete(id);
  }

  async clear(): Promise<void> {
    this.repository.clear();
  }
}
