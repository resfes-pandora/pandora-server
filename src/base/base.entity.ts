import {
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

export class BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @CreateDateColumn({ name: 'created_at' })
  createAt: Date;

  @ApiProperty()
  @UpdateDateColumn({ name: 'update_at' })
  updateAt: Date;

  constructor(data: { [key: string]: any } = {}) {
    Object.keys(data).forEach(key => {
      this[key] = data[key];
    });
  }
}
