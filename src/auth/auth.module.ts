import { Module } from '@nestjs/common';
import { DependencyModule } from 'src/dependency.module';
import { AuthController } from './auth.controller';

@Module({
  imports: [DependencyModule],
  providers: [],
  controllers: [AuthController],
})
export class AuthModule {}
