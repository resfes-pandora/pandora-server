require('dotenv').config();
const { PeerServer } = require('peer');
import { CrudConfigService } from '@nestjsx/crud';

CrudConfigService.load({
  params: {
    id: {
      field: 'id',
      type: 'string',
      primary: true,
    },
  },
  routes: {
    only: [
      'getOneBase',
      'updateOneBase',
      'createOneBase',
      'deleteOneBase',
      'getManyBase',
      'createManyBase',
    ],
  },
});

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

(async () => {
  const peerServer = PeerServer({
    port: +process.env.PEER_PORT || 9000,
    path: '/pandora',
  });

  const app = await NestFactory.create(AppModule);
  const port = process.env.PORT || 3000;
  const options = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('Pandora API')
    .setDescription('Anti cheating exam system')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  app.enableCors();
  await app.listen(port);
})();
