import { ApiProperty } from '@nestjs/swagger';
import { UserLoginDto } from './user-login.dto';

export class UserChangePasswordDto extends UserLoginDto {
  @ApiProperty()
  oldPassword: string;
}
