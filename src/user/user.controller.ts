import { Body, Controller, Delete, Patch } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { UserChangePasswordDto } from './dto/user-change-password.dto';
import { User } from './user.entity';
import { UserService } from './user.service';

@Crud({
  model: {
    type: User,
  },
})
@ApiTags('Users')
@Controller('users')
export class UserController implements CrudController<User> {
  constructor(public service: UserService) { }

  @Patch(':id/changePassword')
  async changePassword(@Body() dto: UserChangePasswordDto): Promise<boolean> {
    return this.service.changePassword(
      dto.username,
      dto.password,
      dto.oldPassword,
    );
  }

  @Delete('clear')
  async clear(): Promise<void> {
    return this.service.clear();
  }
}
