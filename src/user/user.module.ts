import { Module } from '@nestjs/common';
import { DependencyModule } from 'src/dependency.module';
import { UserController } from './user.controller';

@Module({
  imports: [DependencyModule],
  controllers: [UserController],
  providers: [],
})
export class UserModule {}
