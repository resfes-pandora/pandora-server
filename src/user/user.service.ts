/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/base/base.service';
import { Repository, DeepPartial } from 'typeorm';
import { User } from './user.entity';
import { Override, CrudRequest } from '@nestjsx/crud';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService extends BaseService<User> {
  constructor(
    @InjectRepository(User)
    protected readonly repository: Repository<User>,
  ) {
    super(repository);
  }

  @Override()
  async updateOne(
    req: CrudRequest,
    dto: DeepPartial<User>,
  ): Promise<User> {
    delete dto.password;
    return super.updateOne(req, dto);
  }

  async changePassword(
    username: string,
    password: string,
    oldPassword: string,
  ): Promise<boolean> {
    const user = await this.repo.findOne({ username });

    if (user && (await user.comparePassword(oldPassword))) {
      password = await bcrypt.hash(password, 10);
      await this.repo.save({ ...user, password });

      return true;
    }

    return false;
  }
}
