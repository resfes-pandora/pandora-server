import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { ExamModule } from './exam/exam.module';

@Module({
  imports: [TypeOrmModule.forRoot(), AuthModule, UserModule, ExamModule],
  providers: [],
})
export class AppModule {}
