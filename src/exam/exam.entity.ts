import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from 'src/base/base.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class Exam extends BaseEntity {
  @ApiProperty()
  @Column({ unique: true })
  examCode: string;

  @ApiProperty()
  @Column()
  minutes: number;
}