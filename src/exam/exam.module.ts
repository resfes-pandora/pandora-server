import { Module } from '@nestjs/common';
import { DependencyModule } from 'src/dependency.module';
import { ExamController } from './exam.controller';

@Module({
  imports: [DependencyModule],
  providers: [],
  controllers: [ExamController],
})
export class ExamModule {}
