import { Controller, Get, NotFoundException } from '@nestjs/common';
import { Param } from '@nestjs/common/decorators/http/route-params.decorator';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { Exam } from './exam.entity';
import { ExamService } from './exam.service';

@Crud({
  model: {
    type: Exam,
  },
})
@ApiTags('Exams')
@Controller('exams')
export class ExamController implements CrudController<Exam> {
  constructor(public service: ExamService) { }

  @Get(':examCode')
  async getExam(@Param() params): Promise<Exam> {
    const result = await this.service.findOne({ examCode: params.examCode });
    if (!result) throw new NotFoundException();
    return result;
  }
}