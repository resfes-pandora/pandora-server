/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from 'src/base/base.service';
import { Repository } from 'typeorm';
import { Exam } from './exam.entity';

@Injectable()
export class ExamService extends BaseService<Exam> {
  constructor(
    @InjectRepository(Exam)
    protected readonly repository: Repository<Exam>,
  ) {
    super(repository);
  }
}
