import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './user/user.service';
import { User } from './user/user.entity';
import { Exam } from './exam/exam.entity';
import { SocketGateway } from './socket/socket.gateway';
import { Client } from './client/client.entity';
import { ClientService } from './client/client.service';
import { AuthService } from './auth/auth.service';
import { LocalStrategy } from './auth/local.strategy';
import { ExamService } from './exam/exam.service';

const pools = [
  SocketGateway,
  UserService,
  ClientService,
  AuthService,
  LocalStrategy,
  ExamService
];

@Module({
  imports: [TypeOrmModule.forFeature([User, Client, Exam])],
  providers: pools,
  exports: pools,
})
export class DependencyModule {}
